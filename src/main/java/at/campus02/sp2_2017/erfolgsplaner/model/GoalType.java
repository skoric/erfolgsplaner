package at.campus02.sp2_2017.erfolgsplaner.model;

public enum GoalType {
    DAILY,
    WEEKLY
}