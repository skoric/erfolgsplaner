package at.campus02.sp2_2017.erfolgsplaner.model;

import at.campus02.sp2_2017.erfolgsplaner.service.CalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.sql.Date;

@Entity
@Component
@Table(name = "goal")
public class Goal {

    // workaround for injection of calendar service
    @Transient
    static CalendarService calendarService;

    @PostConstruct
    public void init() {
        CalendarService test = Goal.calendarService;
    }


    @Autowired
    public void setCalendarService(CalendarService calendarService) {
        Goal.calendarService = calendarService;
    }
    // END OF workaround for injection of calendar service

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    private GoalType type;

    private String description;

    private Date createdOn;
    private Date lastDone;

    private Integer doneTimes = 0;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OWNER_ID")
    private User owner;


    // custom methods
    public boolean needsToBeDoneToday() {
        Date lastDone = this.getLastDone();

        // if never done always needs to be done
        if (lastDone == null) {
            return true;
        }

        GoalType goalType = this.getType();
        switch (goalType) {
            case DAILY:
                return !calendarService.isSameDayAsToday(lastDone);
            case WEEKLY:
                return !calendarService.isSameWeekAsToday(lastDone);
            default:
                return true;
        }
    }

    public int calculateNumberOfPossibleCompletions(){
        Date createdDate = this.getCreatedOn();
        switch (this.getType()) {
            case WEEKLY:
                return calendarService.possibleWeeksSince(createdDate);
            case DAILY:
                return calendarService.possibleDaysSince(createdDate);
            default:
                return -1;
        }
    }

    // getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastDone() {
        return lastDone;
    }

    public void setLastDone(Date lastDone) {
        this.lastDone = lastDone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GoalType getType() {
        return type;
    }

    public void setType(GoalType type) {
        this.type = type;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getDoneTimes() {
        return doneTimes;
    }

    public void setDoneTimes(Integer doneTimes) {
        this.doneTimes = doneTimes;
    }
}