package at.campus02.sp2_2017.erfolgsplaner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class ErfolgsplanerApplication {

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ErfolgsplanerApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ErfolgsplanerApplication.class, args);
	}
}
