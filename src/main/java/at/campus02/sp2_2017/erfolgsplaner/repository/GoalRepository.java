package at.campus02.sp2_2017.erfolgsplaner.repository;

import at.campus02.sp2_2017.erfolgsplaner.model.Goal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoalRepository extends JpaRepository<Goal, Long> {
}
