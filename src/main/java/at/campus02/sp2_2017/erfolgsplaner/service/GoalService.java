package at.campus02.sp2_2017.erfolgsplaner.service;

import at.campus02.sp2_2017.erfolgsplaner.model.Goal;

public interface GoalService {
    void create(Goal goal);
    void delete(Goal goal);
    void save(Goal goal);
    Goal findById(Long id);
    Goal findById(String id);
}
