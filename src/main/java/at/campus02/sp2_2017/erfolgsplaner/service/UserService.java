package at.campus02.sp2_2017.erfolgsplaner.service;

import at.campus02.sp2_2017.erfolgsplaner.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
