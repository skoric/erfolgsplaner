package at.campus02.sp2_2017.erfolgsplaner.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;

import java.lang.reflect.Method;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

@RunWith(SpringRunner.class)
public class CalendarServiceImplTests {
    private Date today;
    private Date oneWeekAgo;

    // create CalendarServiceImpl instance that is specific to this test
    // prevents that this CalendarServiceImpl is shared between tests througs autoscanning
    @TestConfiguration
    static class CalendarServiceImplTestContextConfiguration {
        @Bean
        public CalendarService calendarService() {
            return new CalendarServiceImpl();
        }
    }

    @Autowired
    private CalendarService calendarService;

    @Before
    public void SetUp() {
        Calendar cal = new GregorianCalendar();

        this.today = new Date(new java.util.Date().getTime());

        cal.add(Calendar.DAY_OF_MONTH, -7);
        this.oneWeekAgo = new Date(cal.getTimeInMillis());
    }

    @Test
    public void whenToday_thenSameDayAsToday() {
        assertThat(calendarService.isSameDayAsToday(this.today)).isTrue();
    }

    @Test
    public void whenToday_thenSameWeekAsToday() {
        assertThat(calendarService.isSameWeekAsToday(this.today)).isTrue();
    }

    @Test
    public void whenOneWeekAgo_thenNotSameDayAsToday() {
        assertThat(calendarService.isSameDayAsToday(this.oneWeekAgo)).isFalse();
    }

    @Test
    public void whenOneWeekAgo_thenSameWeekAsToday() {
        assertThat(calendarService.isSameWeekAsToday(this.oneWeekAgo)).isFalse();
    }

    @Test
    public void whenToday_thenOneDayPossible() {
        assertThat(calendarService.possibleDaysSince(this.today)).isEqualTo(1);
    }

    @Test
    public void whenToday_thenOneWeekPossible() {
        assertThat(calendarService.possibleWeeksSince(this.today)).isEqualTo(1);
    }

    @Test
    public void whenOneWeekAgo_thenEightDaysPossible() {
        assertThat(calendarService.possibleDaysSince(this.oneWeekAgo)).isEqualTo(8);
    }

}
