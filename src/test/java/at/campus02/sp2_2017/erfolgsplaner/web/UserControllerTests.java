package at.campus02.sp2_2017.erfolgsplaner.web;

import at.campus02.sp2_2017.erfolgsplaner.model.User;
import at.campus02.sp2_2017.erfolgsplaner.service.CalendarService;
import at.campus02.sp2_2017.erfolgsplaner.service.GoalService;
import at.campus02.sp2_2017.erfolgsplaner.service.SecurityService;
import at.campus02.sp2_2017.erfolgsplaner.service.UserService;
import at.campus02.sp2_2017.erfolgsplaner.validator.UserValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class UserControllerTests {

    private User testUser;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    @MockBean
    private GoalService goalService;

    @MockBean
    private CalendarService calenderService;

    @MockBean
    UserValidator userValidator;

    @MockBean
    SecurityService securityService;

    @Test
    public void User() throws Exception {
        this.mvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}
